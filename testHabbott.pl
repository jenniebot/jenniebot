#!/usr/bin/env perl

use strict;
use warnings;

use Habbott;

my $h = new Habbott;

$h->LoadConfiguration('bot.cfg');
$h->Start();
