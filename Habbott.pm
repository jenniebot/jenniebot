package Habbott;

use Carp;
use DBI;

use Data::Dumper;

use AnyEvent;
use AnyEvent::IRC::Connection;

use Module::Pluggable require => 1, search_path => [ Habbott::Plugins ]; 

BEGIN {
	our(@EXPORT);

	@EXPORT=qw/new LoadConfiguration Start Stop/;
}

our($config);

our($pluginCallbacks);

sub new ($) {
    my ($class) = @_;

    my ($self) = {
	state => 'stopped',
	config => {},
    };

    bless($self, $class);

    return $self;
};

sub LoadConfiguration {
    my ($self, $file) = @_;
    my (%config);

    open (my $f, "<$file") || croak "Unable to open $file: $!";

    while(<$f>) {
	next if(/^[[:space:]]*#.*$/);
	next if(/^[[:space:]]*$/);

	if(/^[[:space:]]*([^=]+)=[[:space:]]*(.*)[[:space:]]*$/) {
	    $config{lc($1)} = $2;
	}
    }

    $self->{config} = \%config;

    close($f);
};

sub Start {
    my ($self) = @_;
    my ($con) = new AnyEvent::IRC::Connection;
    my ($cv) = AnyEvent->condvar;

    unless ($self->{state} eq 'stopped') {
	carp 'Already trying to run a non-stopped bot';
	return;
    }

    $self->{dbh} = DBI->connect($self->{config}->{dbh});

    $self->SetupCallbacks($con);
    $self->LoadPlugins();

    $self->{cv} = $cv;
    $self->{connection} = $con; 
    $self->{state} = 'running';

    $self->{connection}->connect($self->{config}->{server}, $self->{config}->{port});
    $cv->wait();
};

sub Stop {
    my ($self) = @_;

    unless ($self->{state} eq 'running') {
	carp 'Already trying to stop a non-running bot';
	return;
    }

    $self->{connection}->disconnect;
    $self->{connection} = undef;
    $self->{cv}->broadcast;
    $self->{cv} = undef;
    $self->{state} = 'stopped';
};

sub LoadPlugins {
    my ($self) = @_;

    print Dumper(plugins());
    print "\n";
    foreach my $plugin (plugins()) {
	print 'Loading '.$plugin->name()."\n";
	$plugin->Init($self);
    }
};

sub SetupCallbacks {
    my ($self, $con) = @_;

    $pluginCallbacks = {};

    $con->reg_cb(connect => sub {
	    my($c) = @_;
	    $c->send_msg( USER => $self->{config}->{user}, '*', '0', $self->{config}->{realname} );
	    $c->send_msg( NICK => $self->{config}->{nick});
	    $self->{nick} = $self->{config}->{nick};
	},
	irc_433 => sub {
	    my ($c) = @_;
	    $self->{nick}.='_';
	    $c->send_msg( NICK => $self->{nick});
	},
	irc_376 => sub {
	    my ($c) = @_;
	    $self->InvokePlugins('ready');
	},
	irc_join => sub {
	    my ($c, $params) = @_;
	    $self->InvokePlugins('join', $params);
	},
	irc_ping => sub {
	    my ($c, $params) = @_;
	    $c->send_msg('PONG' => $params->{params}->[0]);
        },
	irc_invite => sub {
	    my ($c, $params) = @_;
	    $self->InvokePlugins('invite',$params);
	}
    );
};

sub InvokePlugins {
    my ($self, $callback, @params) = @_;

    if(exists $pluginCallbacks->{$callback}) {
	foreach my $fptr (@{$pluginCallbacks->{$callback}}) {
	    $fptr->($self, @params);
	 }
    }
};

sub RegisterCallback {
    my ($self, $callback, $plugin, $method) = @_;

    unless(exists $pluginCallbacks->{$callback}) {
	$pluginCallbacks->{$callback} = [];
    }

    unshift $pluginCallbacks->{$callback}, $method;
};

1;
