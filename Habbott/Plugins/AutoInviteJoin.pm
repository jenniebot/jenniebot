package Habbott::Plugins::AutoInviteJoin;

use strict;
use warnings;

use Data::Dumper;

our($dbh);

sub Init
{
    my ($self, $habbott) = @_;
    
    $dbh = $habbott->{dbh};
    $habbott->RegisterCallback( 'invite', '', \&cmd_invite );
};

sub name 
{
    return "AutoInviteJoin plugin.";
};

sub cmd_invite
{
    my ($habbott, $params) = @_;
    my $c = $habbott->{connection};
    my $channel = $params->{params}->[1];
    $c->send_msg('JOIN' => $channel);
};

1;
