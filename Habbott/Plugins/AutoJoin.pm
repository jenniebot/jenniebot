package Habbott::Plugins::AutoJoin;

use strict;
use warnings;

use Data::Dumper;

BEGIN {
	our(@ISA);

	@ISA = qw/Habbott::Plugin/;
}

our($dbh);

our(@CHANNELS) = qw/#shweet #testing/;
our(@autojoinChannels);

sub Init {
	my ($self, $habbott) = @_;

	$dbh = $habbott->{dbh};
	$habbott->RegisterCallback('ready', '', \&cmd_ready);
};

sub name {
	return "AutoJoin plugin";
};

sub cmd_ready {
	my ($habbott) = @_;
	my ($c) = $habbott->{connection};
	my $index = 1;

	foreach my $channel (@CHANNELS) {
	    my $tw = AnyEvent->timer(after => $index,
				     cb => sub {
					 $c->send_msg('JOIN' => $channel);
					 my $t = pop(@autojoinChannels);
					 undef $t;
				     });
	    $index++;
	    unshift @autojoinChannels, $tw;
	}
};

1;
