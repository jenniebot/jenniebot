package Habbott::Plugins::AutoOP;

use strict;
use warnings;

use Data::Dumper;

use Habbott::Utils qw/irc_nick_from_prefix/;

BEGIN {
	our(@ISA);

	@ISA = qw/Habbott::Plugin/;
}

our($dbh);

our(@ADMINS) = qw/writer sweet_kid/;
our(@autojoinChannels);

sub Init {
	my ($self, $habbott) = @_;

	$dbh = $habbott->{dbh};
	$habbott->RegisterCallback('join', '', \&cmd_join);
};

sub name {
	return "AutoOP plugin";
};

sub cmd_join {
	my ($habbott, $params) = @_;
	my ($c) = $habbott->{connection};

	my $nick = Habbott::Utils::irc_nick_from_prefix($params->{prefix});

	foreach(@ADMINS) {
	    if($nick eq $_) {
		$c->send_msg(MODE => $params->{params}->[0], '+o', $nick);
		last;
	    }
	}
};

1;
