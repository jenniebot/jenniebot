package Habbott::Plugins::SeenCommand;

use strict;
use warnings;

use Data::Dumper;

BEGIN {
	our(@ISA);

	@ISA = qw/Habbott::Plugin/;
}

our($dbh);

sub Init {
	my ($self, $habbott) = @_;

	$dbh = $habbott->{dbh};
	$habbott->RegisterCallback('ready', '', \&cmd_ready);
};

sub name {
	return "SeenCommand plugin";
};

sub cmd_ready {
	my ($habbott) = @_;
	my ($c) = $habbott->{connection};

	$c->send_msg(JOIN => '#shweet');
};

1;
