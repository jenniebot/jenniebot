package Habbott::Utils;

BEGIN {
    our(@EXPORT);

    @EXPORT = qw/irc_nick_from_prefix/;
}

sub irc_nick_from_prefix($) {
    my (@prefix) = split(/!/,$_[0]);
    return $prefix[0];
}

1;
